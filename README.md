# learning react
## react+vite
appというreactプロジェクトの作成
```sh
npm create vite@latest app -- --template react-swc-ts
cd app
npm install
```
## eslint
```sh
npm install -D eslint-plugin-import eslint-plugin-unused-imports
```
## prettier
```sh
npm install --save-dev prettier eslint-config-prettier eslint-plugin-prettier
```
**Edit file**
```
```
## tailwind
```sh
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p
```
**Edit file**
```js
// tailwind.config.js
/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [],
}
```
```css
/* index.css */
@tailwind base;
@tailwind components;
@tailwind utilities;
```
## react-router
```sh
npm install react-router-dom localforage match-sorter sort-by
```
## 参考
react+vite
https://zenn.dev/kazukix/articles/react-setup-2024
  
tailwind
https://tailwindcss.com/docs/guides/vite
