import { useState } from "react";

const post = (data: any) => {
  console.log(data);
};

export const Test = () => {
  const [inputArrayForObject, setInputArrayForObject] = useState<string[]>([]);
  const handleAddInputForObject = () => {
    setInputArrayForObject((prev) => [...prev, ""]);
  };
  const handleDeleteInputForObject = (i: number) => {
    if (inputArrayForObject.length > 1) {
      setInputArrayForObject((prev) => {
        const newInputArray = [...prev];
        newInputArray.splice(i, 1);
        return newInputArray;
      });
    }
  };
  const onChangeForObject = (
    e: React.ChangeEvent<HTMLInputElement>,
    i: number,
  ) => {
    setInputArrayForObject((prev) => {
      const newInputArray = [...prev];
      newInputArray[i] = e.target.value;
      return newInputArray;
    });
  };
  const handleSubmitInputForObject = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log("Object");
    post(inputArrayForObject);
  };

  const [inputArrayForNG, setInputArrayForNG] = useState<string[]>([]);
  const handleAddInputForNG = () => {
    setInputArrayForNG((prev) => [...prev, ""]);
  };
  const handleDeleteInputForNG = (i: number) => {
    if (inputArrayForNG.length > 1) {
      setInputArrayForNG((prev) => {
        const newInputArray = [...prev];
        newInputArray.splice(i, 1);
        return newInputArray;
      });
    }
  };
  const onChangeForNG = (e: React.ChangeEvent<HTMLInputElement>, i: number) => {
    setInputArrayForNG((prev) => {
      const newInputArray = [...prev];
      newInputArray[i] = e.target.value;
      return newInputArray;
    });
  };
  const handleSubmitInputForNG = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    let data = [];
    for (const v of e.currentTarget.elements) {
      if (v instanceof HTMLInputElement) {
        data.push(v.value);
      }
    }
    console.log("NG");
    post(data);
  };

  type arrayWithID = {
    id: number;
    value: string;
  };
  const [inputArrayForID, setInputArrayForID] = useState<arrayWithID[]>([]);
  const [currentPrimaryKey, setCurrentPrimaryKey] = useState<number>(0);
  const handleAddInputForID = () => {
    setInputArrayForID((prev) => [
      ...prev,
      {
        id: currentPrimaryKey,
        value: "",
      },
    ]);
    setCurrentPrimaryKey((prev) => prev + 1);
  };
  const handleDeleteInputForID = (i: number) => {
    if (inputArrayForID.length > 1) {
      setInputArrayForID((prev) => {
        const newInputArray = [...prev];
        newInputArray.splice(i, 1);
        return newInputArray;
      });
    }
  };
  const onChangeForID = (e: React.ChangeEvent<HTMLInputElement>, i: number) => {
    setInputArrayForID((prev) => {
      const newInputArray = [...prev];
      newInputArray[i] = {
        id: newInputArray[i].id,
        value: e.target.value,
      };
      return newInputArray;
    });
  };
  const handleSubmitInputForID = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    let data = [];
    for (const v of e.currentTarget.elements) {
      if (v instanceof HTMLInputElement) {
        data.push(v.value);
      }
    }
    console.log("ID");
    post(data);
  };

  return (
    <div>
      <form
        onSubmit={(e) => {
          handleSubmitInputForObject(e);
        }}
      >
        Object
        {inputArrayForObject.map((v, i) => (
          <ul key={i}>
            <li>
              <p>{`index: ${i}`}</p>
              <p>{`value: ${v}`}</p>
              <input
                value={v}
                onChange={(e) => onChangeForObject(e, i)}
                type="text"
              />
              <button
                onClick={() => handleDeleteInputForObject(i)}
                type="button"
              >
                Delete
              </button>
            </li>
          </ul>
        ))}
        <button type="button" onClick={handleAddInputForObject}>
          Add
        </button>
        <button type="submit">Submit Object</button>
      </form>

      <form
        onSubmit={(e) => {
          handleSubmitInputForNG(e);
        }}
      >
        NG
        {inputArrayForNG.map((v, i) => (
          <ul key={i}>
            <li>
              <p>{`index: ${i}`}</p>
              <p>{`value: ${v}`}</p>
              <input
                // value={v}
                onChange={(e) => onChangeForNG(e, i)}
                type="text"
              />
              <button onClick={() => handleDeleteInputForNG(i)} type="button">
                Delete
              </button>
            </li>
          </ul>
        ))}
        <button type="button" onClick={handleAddInputForNG}>
          Add
        </button>
        <button type="submit">Submit NG</button>
      </form>

      <form
        onSubmit={(e) => {
          handleSubmitInputForID(e);
        }}
      >
        ID
        {inputArrayForID.map((v, i) => (
          <ul key={v.id}>
            <li>
              <p>{`index: ${i}`}</p>
              <p>{`value: ${v.value}`}</p>
              <input
                // value={v}
                onChange={(e) => onChangeForID(e, i)}
                type="text"
              />
              <button onClick={() => handleDeleteInputForID(i)} type="button">
                Delete
              </button>
            </li>
          </ul>
        ))}
        <button type="button" onClick={handleAddInputForID}>
          Add
        </button>
        <button type="submit">Submit ID</button>
      </form>
    </div>
  );
};
