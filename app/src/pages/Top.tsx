export const Top = () => {
  let show = "second";
  let list = ["first", "second", "third"];
  return (
    <div className="p-2">
      <div className="relative flex justify-center items-center w-32 h-32">
        {/* 外側の線（より大きな三角形で模擬） */}
        <div className="absolute w-0 h-0 border-l-[57px] border-r-[57px] border-b-[100px] border-l-transparent border-r-transparent border-b-amber-400"></div>
        <div className="absolute w-0 h-0 border-l-[55px] border-r-[55px] border-b-[98px] border-l-transparent border-r-transparent border-b-white"></div>
        {/* 内側の三角形 */}
        <div className="absolute w-0 h-0 border-l-[26px] border-r-[26px] border-b-[38px] border-l-transparent border-r-transparent border-b-amber-400"></div>
        {/* 二重線の間隔を作るために内側の背景色を使用してマスク */}
        <div className="absolute w-0 h-0 border-l-[17px] border-r-[17px] border-b-[30px] border-l-transparent border-r-transparent border-b-amber-500"></div>
      </div>
    </div>
  );
};
