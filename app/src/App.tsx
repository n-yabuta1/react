import { appURL } from "@/config/url";
import { Test } from "@/pages/Test";
import { Top } from "@/pages/Top";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={appURL.top} element={<Top />} />
        <Route path={appURL.test} element={<Test />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
